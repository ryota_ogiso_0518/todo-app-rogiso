require 'rails_helper'

RSpec.describe "Todos", type: :request do

  let!(:todos) { create_list(:todo, 10) }

  describe 'GET /todos' do
    example 'データ一覧取得成功' do
      get '/todos'

      expect(json).to be_present
      expect(json['todos'].size).to eq 10
      expect(response.status).to eq 200
    end

    example 'データ一覧取得失敗' do
      allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 3
      expect(json['error_message']).to eq '一覧の取得に失敗しました'
    end

    example 'その他サーバーエラー' do
      allow(Todo).to receive(:select).and_raise(Exception)
      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'test', detail: 'test' } }

    example 'Todo登録成功' do
      post '/todos', params: valid_attributes

      expect(response.status).to eq 200
      expect(json['error_code']).to eq 0
      expect(json['error_message']).to eq ''
    end

    context 'title最大文字数丁度の場合' do
      let(:params) { { title: 'a' * 100 } }

      example 'Todo登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(Todo.last.title).to eq 'a' * 100
      end
    end

    context 'detail最大文字数丁度の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1000 } }

      example 'Todo登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(Todo.last.detail).to eq 'a' * 1000
      end
    end

    context 'title未入力の場合' do
      let(:invalid_attributes) { { title: '' } }

      example 'Todo登録失敗' do
        post '/todos', params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'title最大文字数＋１の場合' do
      let(:params) { { title: 'a' * 101 } }

      example 'Todo登録失敗' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'detail最大文字数＋１の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1001 } }

      example 'Todo登録失敗' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    example 'サーバーサイドのエラーによる登録失敗' do
      allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
      post '/todos', params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 4
      expect(json['error_message']).to eq '登録に失敗しました'
    end

    example 'その他サーバーエラー' do
      allow(Todo).to receive(:select).and_raise(Exception)
      post '/todos', params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'Running' } }
    let(:todo_id) { todos.first.id }

    example 'リクエストが正常な場合、Todo更新成功' do
      put "/todos/#{todo_id}", params: valid_attributes

      expect(response.status).to eq 200
      expect(json['error_code']).to eq 0
      expect(json['error_message']).to eq ''
    end

    context 'title最大文字数丁度の場合' do
      let(:params) { { title: 'a' * 100 } }

      example 'Todo更新成功' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(Todo.first.title).to eq 'a' * 100
      end
    end

    context 'detail最大文字数丁度の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1000 } }

      example 'Todo更新成功' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
        expect(Todo.first.detail).to eq 'a' * 1000
      end
    end

    context 'title未入力の場合' do
      let(:invalid_attributes) { { title: '' } }

      example 'Todo更新失敗' do
        put "/todos/#{todo_id}", params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'title最大文字数＋１の場合' do
      let(:params) { { title: 'a' * 101 } }

      example 'Todo更新失敗' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'detail最大文字数＋１の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1001 } }

      example 'Todo更新失敗' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'データがない場合' do
      # テストのため事前に該当データを削除
      before { Todo.find(todo_id).destroy }

      example 'Todo更新失敗' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    example 'サーバーサイドのエラーで更新に失敗した場合、Todo更新失敗' do
      allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
      put "/todos/#{todo_id}", params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 5
      expect(json['error_message']).to eq '更新に失敗しました'
    end

    example 'その他のサーバーエラーがあった場合、Todo更新失敗' do
      allow(Todo).to receive(:select).and_raise(Exception)
      put "/todos/#{todo_id}", params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  describe 'DELETE /todos/:id' do
    let(:todo_id) { todos.first.id }

    example '正常な場合、Todo削除成功' do
      delete "/todos/#{todo_id}"

      expect(response.status).to eq 200
      expect(json['error_code']).to eq 0
      expect(json['error_message']).to eq ''
    end

    context 'idがない場合' do
      # 事前に該当レコードを削除
      before { Todo.find(todo_id).destroy }

      example 'Todo削除失敗' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    example 'サーバーサイドのエラーで削除失敗が起きた場合、Todo削除失敗' do
      allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
      delete "/todos/#{todo_id}"

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 6
      expect(json['error_message']).to eq '削除に失敗しました'
    end

    example 'その他サーバーエラー' do
      allow(Todo).to receive(:select).and_raise(Exception)
      delete "/todos/#{todo_id}"

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end
end
