module ExceptionHandler

  extend ActiveSupport::Concern

  included do
    rescue_from Exception, with: :render_server_error
    rescue_from ActiveRecord::RecordInvalid, with: :render_request_error
    rescue_from ActiveRecord::RecordNotFound, with: :render_request_error
  end

  SERVER_ERROR_CODE = 1
  REQUEST_ERROR_CODE = 2

  def render_server_error
    render json: { error_code: SERVER_ERROR_CODE, error_message: 'サーバー内で不明なエラーが発生しました' }, status: 500
  end

  def render_request_error
    render json: { error_code: REQUEST_ERROR_CODE, error_message: 'リクエストの形式が不正です' }, status: 400
  end
end