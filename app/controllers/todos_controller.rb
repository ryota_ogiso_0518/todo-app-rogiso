class TodosController < ApplicationController

  before_action :set_todo, only: [:update, :destroy]

  NO_ERROR_CODE = 0
  GET_LIST_ERROR_CODE = 3
  REGISTRATION_ERROR_CODE = 4
  UPDATE_ERROR_CODE = 5
  DELETE_ERROR_CODE = 6

  def index
    # Todoリストの取得
    todo_list = Todo.select(:id, :title, :detail, :date).order(:date)
    render json: { todos: todo_list, error_code: NO_ERROR_CODE, error_message: '' },　status: 200
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: GET_LIST_ERROR_CODE, error_message: '一覧の取得に失敗しました' }, status: 500
  end

  def create
    # Todoレコードの作成
    Todo.create!(todo_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_request_error
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: REGISTRATION_ERROR_CODE, error_message: '登録に失敗しました' }, status: 500
  end

  def update
    # Todoレコードの更新
    @todo.update!(todo_params)
    render_success
  rescue ActiveRecord::RecordInvalid
    render_request_error
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: UPDATE_ERROR_CODE, error_message: '更新に失敗しました' }, status: 500
  end

  def destroy
    # Todoレコードの削除
    @todo.destroy!
    render_success
  rescue ActiveRecord::ActiveRecordError
    render json: { error_code: DELETE_ERROR_CODE, error_message: '削除に失敗しました' }, status: 500
  end

  private

  def set_todo
    @todo = Todo.find(params[:id])
  end

  def todo_params
    params.permit(:title, :detail, :date)
  end

  def render_success
    render json: { error_code: NO_ERROR_CODE, error_message: '' }, status: 200
  end
end
